package jhonatan.sabadi.inchurch.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import jhonatan.sabadi.inchurch.api.call.MovieApi
import jhonatan.sabadi.inchurch.api.retrofit.RetrofitService

@InstallIn(ActivityComponent::class)
@Module
object RetrofitModule {

    @Provides
    fun provideMovieApi(): MovieApi =
        RetrofitService.createService(
            MovieApi::class.java
        )

}